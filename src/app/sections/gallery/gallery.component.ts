import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
})
export class GalleryComponent implements OnInit {
  images = [
    'image-gallery-milkbottles.jpg',
    'image-gallery-orange.jpg',
    'image-gallery-cone.jpg',
    'image-gallery-sugarcubes.jpg',
  ];

  constructor() {}

  ngOnInit(): void {}

  getimagePath(imageName: string) {
    const type = window.innerWidth <= 600 ? 'mobile' : 'desktop';
    return `../../../../assets/img/${type}/${imageName}`;
  }
}
