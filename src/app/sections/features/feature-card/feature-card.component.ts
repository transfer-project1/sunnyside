import { Component, Input, OnInit } from '@angular/core';
import { FeatureCard } from '../features.component';

@Component({
  selector: 'app-feature-card',
  templateUrl: './feature-card.component.html',
  styleUrls: ['./feature-card.component.scss'],
})
export class FeatureCardComponent implements OnInit {
  @Input() feature!: FeatureCard;

  constructor() {}

  ngOnInit(): void {}

  get imagePath() {
    const type = window.innerWidth <= 600 ? 'mobile' : 'desktop';
    return `url(../../../../assets/img/${type}/${this.feature.imageName})`;
  }
}
