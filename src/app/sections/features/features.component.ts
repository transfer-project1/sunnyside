import { Component, OnInit } from '@angular/core';

export type FeatureRow = {
  heading: string;
  description: string;
  learnMoreColor: string;
  imageName: string;
  direction: 'ltr' | 'rtl';
};

export type FeatureCard = {
  heading: string;
  description: string;
  fontColor: string;
  imageName: string;
};

@Component({
  selector: 'app-features',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.scss'],
})
export class FeaturesComponent implements OnInit {
  featureRows: FeatureRow[] = [
    {
      heading: 'Transform your brand',
      description:
        'We are a full-service creative agency specializing in helping brands grow fast. Engage your clients through compelling visuals that do most of the marketing for you.',
      learnMoreColor: 'yellow',
      imageName: 'image-transform.jpg',
      direction: 'ltr',
    },
    {
      heading: 'Stand out to the right audience',
      description:
        'Using a collaborative formula of designers, researchers, photographers, videographers, and copywriters, we’ll build and extend your brand in digital places. ',
      learnMoreColor: 'red',
      imageName: 'image-stand-out.jpg',
      direction: 'rtl',
    },
  ];
  featureCards: FeatureCard[] = [
    {
      heading: 'Graphic design',
      description:
        'Great design makes you memorable. We deliver artwork that underscores your brand message and captures potential clients’ attention.',
      fontColor: 'cyan',
      imageName: 'image-graphic-design.jpg',
    },
    {
      heading: 'Photography',
      description:
        'Increase your credibility by getting the most stunning, high-quality photos that improve your business image.',
      fontColor: 'blue',
      imageName: 'image-photography.jpg',
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
