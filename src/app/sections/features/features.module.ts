import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeaturesComponent } from './features.component';
import { FeatureRowComponent } from './feature-row/feature-row.component';
import { FeatureCardComponent } from './feature-card/feature-card.component';

@NgModule({
  declarations: [FeaturesComponent, FeatureRowComponent, FeatureCardComponent],
  imports: [CommonModule],
  exports: [FeaturesComponent],
})
export class FeaturesModule {}
