import { Component, Input, OnInit } from '@angular/core';
import { FeatureRow } from '../features.component';

@Component({
  selector: 'app-feature-row',
  templateUrl: './feature-row.component.html',
  styleUrls: ['./feature-row.component.scss'],
})
export class FeatureRowComponent implements OnInit {
  @Input() feature!: FeatureRow;

  constructor() {}

  ngOnInit(): void {}

  get imagePath() {
    const type = window.innerWidth <= 600 ? 'mobile' : 'desktop';
    return `../../../../assets/img/${type}/${this.feature.imageName}`;
  }
}
