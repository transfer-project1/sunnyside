import { Component, Input, OnInit } from '@angular/core';
import { Testimonial } from '../testimonials.component';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() testimonial!: Testimonial;

  constructor() {}

  ngOnInit(): void {}

  get imageName() {
    return `../../../assets/img/${this.testimonial.imageName}`;
  }
}
