import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestimonialsComponent } from './testimonials.component';
import { CardComponent } from './card/card.component';

@NgModule({
  declarations: [TestimonialsComponent, CardComponent],
  imports: [CommonModule],
  exports: [TestimonialsComponent],
})
export class TestimonialsModule {}
