import { Component, OnInit } from '@angular/core';

export type Testimonial = {
  imageName: string;
  testimony: string;
  personName: string;
  personRole: string;
};

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.scss'],
})
export class TestimonialsComponent implements OnInit {
  testimonials: Testimonial[] = [
    {
      imageName: 'image-emily.jpg',
      testimony:
        'We put our trust in Sunnyside and they delivered, making sure our needs were met and deadlines were always hit.',
      personName: 'Emily R.',
      personRole: 'Marketing Director',
    },
    {
      imageName: 'image-thomas.jpg',
      testimony:
        'Sunnyside’s enthusiasm coupled with their keen interest in our brand’s success made it a satisfying and enjoyable experience.',
      personName: 'Thomas S.',
      personRole: 'Chief Operating Officer',
    },
    {
      imageName: 'image-jennie.jpg',
      testimony:
        'Incredible end result! Our sales increased over 400% when we worked with Sunnyside. Highly recommended!',
      personName: 'Jennie F.',
      personRole: 'Business Owner',
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
