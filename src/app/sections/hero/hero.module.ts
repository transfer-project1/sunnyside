import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeroComponent } from './hero.component';
import { NavbarModule } from 'src/app/widgets/navbar/navbar.module';

@NgModule({
  declarations: [HeroComponent],
  imports: [CommonModule, NavbarModule],
  exports: [HeroComponent],
})
export class HeroModule {}
