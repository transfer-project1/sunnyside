import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  links = [
    {
      label: 'About',
      link: '#',
    },
    {
      label: 'Services',
      link: '#',
    },
    {
      label: 'Projects',
      link: '#',
    },
  ];
  icons = [
    {
      name: 'facebook',
      link: '#',
    },
    {
      name: 'instagram',
      link: '#',
    },
    {
      name: 'twitter',
      link: '#',
    },
    {
      name: 'pinterest',
      link: '#',
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
