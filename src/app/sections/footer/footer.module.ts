import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer.component';
import { LogoModule } from 'src/app/widgets/logo/logo.module';
import { SvgIconModule } from 'src/app/widgets/svg-icon/svg-icon.module';

@NgModule({
  declarations: [FooterComponent],
  imports: [CommonModule, LogoModule, SvgIconModule],
  exports: [FooterComponent],
})
export class FooterModule {}
