import { Component, OnInit } from '@angular/core';

export type Link = {
  label: string;
  link: string;
};

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  links: Link[] = [
    {
      label: 'About',
      link: '#',
    },
    {
      label: 'Services',
      link: '#',
    },
    {
      label: 'Projects',
      link: '#',
    },
    {
      label: 'Contact',
      link: '#',
    },
  ];
  sideMenuIsOpen: boolean | null = null;

  constructor() {}

  ngOnInit(): void {}

  toggleSideMenu() {
    this.sideMenuIsOpen = !this.sideMenuIsOpen;
  }
}
