import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar.component';
import { LogoModule } from '../logo/logo.module';
import { NavLinksComponent } from './nav-links/nav-links.component';

@NgModule({
  declarations: [NavbarComponent, NavLinksComponent],
  imports: [CommonModule, LogoModule],
  exports: [NavbarComponent],
})
export class NavbarModule {}
