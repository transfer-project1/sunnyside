import { Component, Input, OnInit } from '@angular/core';
import { Link } from '../navbar.component';

@Component({
  selector: 'app-nav-links',
  templateUrl: './nav-links.component.html',
  styleUrls: ['./nav-links.component.scss'],
})
export class NavLinksComponent implements OnInit {
  @Input() links!: Link[];

  constructor() {}

  ngOnInit(): void {}
}
