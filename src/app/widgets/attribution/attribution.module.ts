import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttributionComponent } from './attribution.component';

@NgModule({
  declarations: [AttributionComponent],
  imports: [CommonModule],
  exports: [AttributionComponent],
})
export class AttributionModule {}
