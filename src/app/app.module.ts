import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FeaturesModule } from './sections/features/features.module';
import { FooterModule } from './sections/footer/footer.module';
import { GalleryModule } from './sections/gallery/gallery.module';
import { HeroModule } from './sections/hero/hero.module';
import { TestimonialsModule } from './sections/testimonials/testimonials.module';
import { AttributionModule } from './widgets/attribution/attribution.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HeroModule,
    AttributionModule,
    FeaturesModule,
    TestimonialsModule,
    GalleryModule,
    FooterModule,
    AttributionModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
